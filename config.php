<?php

    $config = [
    
        'main-theme'    => 'default',


        'site-strings'  => [
                                'site-title'        => 'Untitled',
                                'footer-contact'    => '221B Baker Street<br> London NW1',
                                'footer-copyright'  => '&copy; 2016 Meltsi CMS'
                            ],
                            
        '404-page'      => '404/',
        
        
        'base-dir'      => '/'
    
    ];
