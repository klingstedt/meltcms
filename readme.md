Meltsi CMS v.0.2
================

Super-lightweight CMS using PHP.

Main features
-------------

- flexible and simple template enginge
- just files: no database

Directory structure
-------------------

The root level directories and files are:


- content:          all user editable content
- themes:           all layout (edit with care)
- system:           cms system files (no editing)


- index.php:        main system file (no editing)
- config.php:       site configuration file (edit with care)


Content directory structure
---------------------------

The content directory contains all user editable files.
Supported files are **html files**, **image files** (jpg,png,gif) and **configuration files** (config.php).
The structure is as follows:

- pages:            sub-directories for pages (The default name for the home home page directory is "index")
- navigation:       directory for site navigation components
		

Pages directory structure
-------------------------

Each page is a separate sub-diractory in the "content/pages" directory.
The structure is as follows:

- index.html:       main html content of page
- config.php:       page specific configuration (eg. plugins included). See instructions below.


**Plugin files**

Depending on the theme, it may be possible to use plugins on the page.
These are separate files or sub-directories in the page directory. 
In order to appear on the page, plugins must be defined in the page specific configuration file (config.php).

Some examples of plugins:

- *.html:           custom html content
- columns:          directory of articles (*.html files) placed in columns
- gallery:          directory of gallery images to be displayed on page

