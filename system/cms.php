<?php
    
    class cms {
        
        const SYSTEM_DIR_DEFAULT = "system/";
        const THEME_DIR_DEFAULT = "themes/";
        const CONTENT_DIR_DEFAULT = "content/";
        const PAGES_DIR_DEFAULT = "content/pages/";
        const TEMPLATE_DIR_NAME = "templates/";
        const ASSETS_DIR_NAME = "assets/";
        
        const CONFIG_FILE_NAME = "config";
        
        const ERROR_LOG = "system/log/errors";
        
        const HOMEPAGE_NAME = "index";
        
        const PAGE_CONTENT_FILENAME = "index.html";
        
        public $config = array();
        
        public function __construct() {
            
        }
        
        public function loadConfig() {
            //load root config file
            $this->loadConfigFile(self::CONFIG_FILE_NAME, false);
            
            
            if (isset($this->config['base-dir'])) {
                $this->url_base = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->config['base-dir'];
            } elseif (isset($this->config['base-url'])) {
                $this->url_base = $this->config['base-url'];
            } else {
                //parse_url($_SERVER['REQUEST_URI'];, PHP_URL_PATH)
                $this->url_base = 'http://' . $_SERVER['SERVER_NAME'] ;
            }
            
            
            //test the config.. (all nonoptional parameters should have fallback values!)
            
        }
        
        public function loadTheme() {
            
            //set theme files
            if (!isset($this->config['main-theme'])) {
                throw new ThemeException('No theme set');
            }
            
            $this->theme_dir = self::THEME_DIR_DEFAULT . $this->config['main-theme'] . "/";
            $this->template_path = $this->theme_dir . self::TEMPLATE_DIR_NAME;
            $this->assets_path = $this->theme_dir . self::ASSETS_DIR_NAME;
            
            //load theme config file
            $this->loadConfigFile($this->theme_dir . self::CONFIG_FILE_NAME, false);
            
        }
        
        public function loadPage() {
            
            //get the page path
            if (!$this->page_path = $this->getPagePath()) {
                throw new PageException('Invalid page path');
            }
            
            //load page config file
            $this->loadConfigFile($this->page_path . self::CONFIG_FILE_NAME, true);
            
        }
        
        public function render() {
            $html = "";
            
            //load template files
            foreach ($this->config['templates'] as $template => $template_attr) {
                if (!$template_content = file_get_contents($this->template_path . $template)) {
                    throw new ThemeException('Template file not found: ' . $template);
                }
                //append
                $html .= $template_content;
            }
            // load page contents
            if (!$page_html =  file_get_contents($this->page_path . self::PAGE_CONTENT_FILENAME)) {
                throw new PageException('Page content not found: ' . $this->page_path . self::PAGE_CONTENT_FILENAME);
            }
            
            // replacement of {% page-content %}
            $html=$this->replaceSystemVariable('page-content',$page_html,$html);
            
            // load navigation contents
            if (!$navigation_html =  file_get_contents(self::CONTENT_DIR_DEFAULT . 'navigation/' . self::PAGE_CONTENT_FILENAME)) {
                //throw new Exception('Page content not found: ' . $this->page_path . self::PAGE_CONTENT_FILENAME);
                $navigation_html = "";
            }
            
            // replacement of {% page-content %}
            $html=$this->replaceSystemVariable('navigation',$navigation_html,$html);
            
            
            // load page plugins 
            $plugins_html = "";
                if (isset($this->config['page-plugins'])) {
                    foreach ($this->config['page-plugins'] as $plugin_name => $plugin_type) {
                        $plugin_path = $this->page_path . $plugin_name;
                        
                        if (!isset($this->config['theme-plugins'][$plugin_type])) {
                            throw new PluginException('Unknown plugin type: '. $plugin_type);
                            return false;
                        }
                        $plugin_type_config = $this->config['theme-plugins'][$plugin_type];
                        
                        $plugin = new Plugin($plugin_path, $plugin_type_config, $this->template_path);
                        $plugins_html .= $plugin->renderPlugin();
                    }
                }
            // replacement of {% page-plugins %}
            $html=$this->replaceSystemVariable('page-plugins', $plugins_html, $html);
            
            // replacement of {% asset-path %},
            $html=$this->replaceSystemVariable('asset-path', $this->url_base . $this->assets_path, $html);
            
            // replacement of {% page-path %},
            $html=$this->replaceSystemVariable('page-path', $this->url_base . $this->page_path,$html);
            
            // replacement of {% site-url %},
            $html=$this->replaceSystemVariable('site-url', $this->url_base, $html);
            
            
            $html=$this->replaceSystemFunction('rand', 'rand' ,$html);
            
            
            
            //replacement of strings:
            $html=$this->replaceStrings('theme-strings', $html);
            $html=$this->replaceStrings('site-strings', $html);
            $html=$this->replaceStrings('page-strings', $html);
            
            echo $html;
        }
        
        public function render404() {
            //set headers
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
            
            //set pagepath
            if (isset($this->config['404-page'])) {
                $this->page_path = self::PAGES_DIR_DEFAULT . $this->config['404-page'];
            } else {
                $this->page_path = self::SYSTEM_DIR_DEFAULT . 'assets/404/';
            }
            //call render
            
            $this->render();
        }
        
        public function logException($e){
            $message = date("Y-m-d H:i:s") . "\n" . $e . "\n\n";
            
            if (is_writable(self::ERROR_LOG)) {
                error_log($message, 3, self::ERROR_LOG);
            } else {
                error_log($message);
            }
        }
        
        private function replaceStrings($string_type,$html){
            //prepare replacement patterns
            $strings = (isset($this->config[$string_type])) ? $this->config[$string_type] : array();
            $keypatterns = array();
            $values = array();
            foreach ($strings as $key => $string){
                $keypatterns[] = preg_replace("/([a-zA-Z-]+)/i","/([{]{2}\s*$1\s*[}]{2})/i",$key);
                $values[]=$string;
            }
            return preg_replace($keypatterns,$values,$html);
        }
        
        private function replaceSystemVariable($func_name, $replacement, $html) {
            return preg_replace("/({%\s*".$func_name."\s*%})/i",$replacement,$html);
        }
        
        private function replaceSystemFunction($func_name, $php_func, $html,$args_expect=2) {
            
            if (preg_match_all("/{%\s*".$func_name."\s*((?:.*\s*)*)\s*%}/iU",$html,$matches)) {
                $n_found = sizeof($matches[0]);
                for ($n = 0 ; $n<$n_found; $n++) {
                    if (isset($matches[1][$n])) {
                        //has argument
                        if (preg_match_all("/\s*(\S+\s*)/iU",$matches[1][$n], $args)) {
                            $args_found = sizeof($args[0]);
                            if ($args_found == $args_expect) {
                                $argstring = implode(",",$args[1]);
                                $replacement = call_user_func_array($php_func,$args[1]);
                            } else {
                                $replacement="";
                            }
                        }
                    }
                    $html = preg_replace("/({%\s*".$func_name."\s*.*\s*%})/iU",$replacement,$html,1);
                }
            } else {
                $replacement = "";
                $html = preg_replace("/({%\s*".$func_name."\s*.*\s*%})/iU",$replacement,$html);
            }
            return $html;
            
            
            
        }
        
        private function getPagePath() {
            if (isset($_GET['p'])) {
                $search_path = $_GET['p'];
            } else {
                $search_path = self::HOMEPAGE_NAME;
            }
            $page_path = self::PAGES_DIR_DEFAULT . $search_path ;
            $page_path .= (substr($page_path, -1)=="/") ? "" : "/";
            
            if (!is_dir($page_path)) {
                throw new PageException('Page not found');
                return false;
            }
            return $page_path;
        }
        
        private function loadConfigFile($file, $optional = false) {
            
            //look for new config in php or json file
            if (is_file($file . '.php')) {
                if (include_once($file . '.php')) {
                    $new_config = $config;
                } else {
                    throw new ConfigException("Invalid configuration file: " . $file);
                    return false;
                }
            } elseif (is_file($file . '.json')) {
                if ($new_config = json_decode(file_get_contents($file . '.json'),true)) {
                    
                } else {
                    throw new ConfigException("Invalid configuration file: " . $file);
                    return false;
                }
            } else {
                if (!$optional) {
                    throw new ConfigException("Configuration file not found: " . $file);
                }
                return false;
            }
            

            // append new config
            if ($this->config = array_replace($this->config, $new_config)) {
                return true;
            } else {
                throw new ConfigException("Failed to append configuration: " . $file);
            };
        }
    }
