<?php
    include "cms.php";
    include "plugin.php";
    include "exceptions.php";

    $page_not_found = FALSE;
    $has_errors = FALSE;
    
    $cms = new cms();


    try {
        $cms->loadConfig();
    } catch (Exception $e) {
        $cms->logException($e);
        $has_errors = TRUE;
    }
    try {
        $cms->loadTheme();
    } catch (Exception $e) {
        $cms->logException($e);
        $has_errors = TRUE;
    }
    try {
        $cms->loadPage();
    } catch (Exception $e) {
        if ($e instanceof PageException) {
            $page_not_found = TRUE;
        } else {
            $cms->logException($e);
            $has_errors = TRUE;
        }
    }
        
    
    //run further tests? redirect if errors loading cms?
    //not implemented
    
    //render page
    
    if (!$page_not_found && !$has_errors) {
        try {
            $cms->render();
        } catch (Exception $e) {
            if ($e instanceof PageException) {
                $page_not_found = TRUE;
            } else {
                $cms->logException($e);
                $has_errors = TRUE;
            }
        }
    }
    
    if ($page_not_found) {
        //render 404
        $cms->render404();
    } elseif ($has_errors) {
        //force render anyway
        //at this point errors have been logged
        $cms->render();
    }
