<?php
    class Plugin {
        
        public function __construct($plugin_file_path, $plugin_type_config, $template_path) {
            $this->path = $plugin_file_path;
            $this->config = $plugin_type_config;
            
            $this->plugin_template = $template_path . $this->config['plugin-template'];
            
            if (isset($this->config['element-template'])) {
                $this->element_template = $template_path . $this->config['element-template'];
            }
            
            if (!(is_file($this->path)||is_dir($this->path))) {
                throw new PluginException('Plugin content file missing: ' . $this->path);
            }
        }
        
        public function renderplugin() {
            
            if (!$plugin_html = file_get_contents($this->plugin_template)) {
                throw new PluginException('Plugin template file not found: ' . $this->template_path);
            }
            if (is_dir($this->path)) {
                $element_paths = glob($this->path . '/*.{jpg,png,gif,html}', GLOB_BRACE);
            } else {
                $element_paths[] = $this->path;
            }
            
            // test if plugin has an element loop
            if (preg_match_all("/{%\s*element-begin\s*%}(.*){%\s*element-end\s*%}/sU", $plugin_html, $matches)) {
                //plugin may have have multiple positions for element loop: distribute elements evenly to positions!
                $n_element_positions = sizeof($matches[0]);   //number of positions
                $n_elements = sizeof($element_paths);       //number of elements
                for ($pos=0; $pos<$n_element_positions; $pos++) {
                    $plugin_html = preg_replace("/({%\s*element-begin\s*%}.*{%\s*element-end\s*%})/sU", "{% element-position %}", $plugin_html,1);
                    $n_elements_in_position = floor($n_elements/$n_element_positions) + ($n_elements%$n_element_positions>$pos ? 1 : 0);
                    $elements_html = "";
                    for ($n=0; $n < $n_elements_in_position; $n++) {
                        $element = $n * $n_element_positions + $pos;
                        $element_content = $this->renderPluginElement($element_paths[$element],$this->config['file-type']);
                        $elements_html .= preg_replace("/({%\s*element-content\s*%})/i",$element_content,$matches[1][$pos]);
                    }
                    
                    $plugin_html = preg_replace("/({%\s*element-position\s*%})/i",$elements_html,$plugin_html);
                }
            } else {
                //no loop tags. only use one element to replace {$ plugin-content $}
            }
            return $plugin_html;
        }
        private function renderPluginElement($path,$file_type) {
            switch ($file_type) {
                case 'html':
                    if (!$element_html = file_get_contents($path)) {
                        throw new PluginException('Plugin file empty: ' . $path);
                    }
                    return $element_html;
                    break;
                case 'asset-url':
                    return $path;
                    break;
            }
        }
    }
